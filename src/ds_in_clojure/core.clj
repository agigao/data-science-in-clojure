(ns ds-in-clojure.core
  (:gen-class :main true)
  (:use [clojure.tools.cli :only (cli)])
  (:require [ds-in-clojure.homework :as hw]))

(defn run
  "Print out the options and the arguments"
  [opts args]
  (println (hw/pollution-stats (get opts :pollutant)
                               (read-string (get opts :start))
                               (read-string (get opts :end)))))

(defn -main [& args]
  (let [[opts args banner]
        (cli args
             ["-h" "--help" "Show help" :flag true :default false]
             ["-p" "--pollutant" "provide pollutant: nitrate or sulfate" :default false]
             ["-s" "--start" "start ID of Monitor" :default 0]
             ["-e" "--end" "end ID of Monitor" :default 20])]
    (when (:help opts)
      (println banner)
      (System/exit 0))
    (if
        (and
         (:pollutant opts)
         (:start opts)
         (:end opts))
      (do
        (println "")
        (run opts args))
      (println banner))))
