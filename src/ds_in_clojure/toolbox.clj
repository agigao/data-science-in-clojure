(ns ds-in-clojure.toolbox
  "My personal toolbox of Clojure functions for Data Science"
  (use [incanter core]))

(defn mean
  [coll]
  (/ (reduce + coll)
     (count coll)))

(defn median
  [coll]
  (let [n (count coll)
        mid (int (/ n 2))]
    (if (odd? n)
      (nth (sort coll) mid)
      (->> (sort coll)
           (drop (dec mid))
           (take 2)
           (mean)))))

(defn variance
  [coll]
  (let [coll-bar (mean coll)
        n (count coll)
        square-deviation (fn [x]
                           (sq (- x coll-bar)))]
    (mean (map square-deviation coll))))

(defn sd
  [coll]
  (sqrt (variance coll)))

(defn quantile
  [q coll]
  (let [n (dec (count coll))
        i (-> (* n q)
              (+ 1/2)
              (int))]
    (nth (sort coll) i)))

(defn bin
  [n-bins coll]
  (let [min-x (apply min coll)
        max-x (apply max coll)
        range-x (- max-x min-x)
        bin-fn (fn
                 [x]
                 (-> x
                     (- min-x)
                     (/ range-x)
                     (* n-bins)
                     (int)
                     (min (dec n-bins))))]
    map bin-fn coll))

