(ns ds-in-clojure.homework
  (:use [incanter core io datasets stats charts])
  [:require [ds-in-clojure.toolbox :as tool]
            [clojure.string :as str]
            [clojure.java.io :as io]])

(defmulti load-data identity)

(defmethod load-data :data [_]
  (read-dataset "specdata/data.csv"))

(head (load-data))

(-> (io/reader "data/specdata/data.csv")
    (line-seq)
    (count))

(defn read-data
  [start-id end-id]
  (->> (read-dataset "data/specdata/data.csv"
                :header true)
       ($where {:ID {:gt start-id :lt end-id}})))

; Part I
(defn pollution-stats
  [pollutant start end]
  (let [data (->> (read-data start end)
                  ($where {:sulfate {:ne "NA"}
                          :nitrate {:ne "NA"}})
                  ($ pollutant))]
    (println "Mean:  " (tool/mean data))
    (println "SD:    " (tool/sd data))))

; Part II
(defn count-na
  [data]
  (->> ($where {:sulfate {:eq "NA"}
                :nitrate {:eq "NA"}}
               data)
       ($ :ID)
       (frequencies)
       (sort)
       (to-dataset)))

(defn count-clean
  [data]
  (->> ($where {:sulfate {:$ne "NA"}
                :nitrate {:$ne "NA"}}
               data)
       ($ :ID)
       (frequencies)
       (sort)
       (to-dataset)
       ($ 1)))

(defn percent
  [x y]
  (int (* (float (/ x y)) 100)))

(defn data-stats
  "Returns a dataset with
   Monitor ID | Clean data | NAs | Percentage"
  [start end]
  (let [data  (read-data start end)
        na    (count-na data)
        clean (count-clean data)]
    (->> (conj-cols na clean)
         (rename-cols {:col-0 :ID
                       :col-1 :NAs
                       :col-2 :Clean-Data})
         (add-derived-column "%-Of-Clean-Data" [:Clean-Data :NAs] percent))))