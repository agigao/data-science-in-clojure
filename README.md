# Data Science in Clojure

Here goes my data-science class coding assigments written in Clojure instead of R.
Repository includes my `toolbox` namespace that includes manual implementation of some fundamental functions for statistics for example: `mean` `median` `standard-deviation` etc.

Application includes and works on 80000 lines of csv data, so, it takes some time to run.
If you want to benchmark functions, simply use wrap any function call into `(time )`.

P.S. It's just a starting point, reading speed and optimization will get better in a meantime.
There's no need to load the whole file into memory each time you execute a function.


## Installation

Download and install [https://leiningen.org](https://leiningen.org)

macOS shortcut for installation:
`brew install leiningen`

## Jar

Cooking distributable standalon jar is as simple as that
 
 `lein uberjar`


## Options

cd into project directory and
`lein run`
displays different options to provide

 ```
 Switches               Default  Desc              
 --------               -------  ----              
 -h, --no-help, --help  false    Show help         
 -p, --pollutant        false    provide pollutant: nitrate or sulfate  
 -s, --start            0        start ID of Monitor          
 -e, --end              20       end ID of Monitor
 ```

## Examples

```lein run -p sulfate -s 37 -e 248```

```java -jar ds-in-clojure-0.1.0-standalone.jar -p nitrate -s 216 -e 384```

## Additional Tricks
in a specdata directory, we had around 300 csv files, and data was merged into one file using bash:
```bash
mv 001.csv 001
# FNR > 1: all the rows except one - to avoid all header duplication
awk 'FNR > 1' *.csv > data.csv
cat data.csv > 001
```

## Bugs
In a jar file directory of data.csv is different, so right now jar file doesn't work literally.
I'll find a workaround in the following days.
Frankly, including data into jar is a silly idea. Pointer argument to data is much better.

## License

Copyright © 2016 Giga Chokheli